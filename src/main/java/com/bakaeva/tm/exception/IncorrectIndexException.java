package com.bakaeva.tm.exception;

public class IncorrectIndexException extends RuntimeException {

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }

}
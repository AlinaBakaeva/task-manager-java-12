package com.bakaeva.tm.exception;

public class EmptyDescriptionException extends RuntimeException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}
package com.bakaeva.tm.api.service;

import com.bakaeva.tm.model.Command;

public interface ICommandService {

    public Command[] getTerminalCommands();

    public String[] getCommands();

    public String[] getArguments();

}
